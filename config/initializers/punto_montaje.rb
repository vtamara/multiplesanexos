Minsip::Application.config.relative_url_root = ENV.fetch(
  'RUTA_RELATIVA', '/multiplesanexos')
Minsip::Application.config.assets.prefix = ENV.fetch(
  'RUTA_RELATIVA', '/multiplesanexos') == '/' ?
 '/assets' : (ENV.fetch('RUTA_RELATIVA', '/multiplesanexos') + '/assets')
