class CreateSipAnexoPersona < ActiveRecord::Migration[7.0]
  def change
    create_table :sip_anexo_persona do |t|
      t.integer :anexo_id
      t.integer :persona_id

      t.timestamps
    end

    add_foreign_key :sip_anexo_persona, :sip_anexo, column: :anexo_id
    add_foreign_key :sip_anexo_persona, :sip_persona, column: :persona_id
  end
end
