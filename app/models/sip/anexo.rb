require 'sip/concerns/models/anexo'

module Sip
  class Anexo < ActiveRecord::Base

    has_many :anexo_persona, foreign_key: :anexo_id,
      validate: true, class_name: 'Sip::AnexoPersona'
    has_many :persona, class_name: 'Sip::Persona', through: :anexo_persona

  end
end

