module Sip
  class AnexoPersona < ActiveRecord::Base

    belongs_to :persona, foreign_key: :persona_id,
      class_name: 'Sip::Persona'
    belongs_to :anexo, foreign_key: :anexo_id,
      class_name: 'Sip::Anexo'

    accepts_nested_attributes_for :anexo, reject_if: :all_blank

    validates :persona, presence: true
    validates :anexo, presence: true
  end
end

