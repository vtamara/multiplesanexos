require 'sip/concerns/models/persona'

module Sip
  class Persona < ActiveRecord::Base
    include Sip::Concerns::Models::Persona

    has_many :anexo_persona, dependent: :delete_all,
      class_name: 'Sip::AnexoPersona',
      foreign_key: 'persona_id', validate: true
    accepts_nested_attributes_for :anexo_persona, 
      allow_destroy: true, reject_if: :all_blank
    has_many :sip_anexo, :through => :anexo_persona, 
      class_name: 'Sip::Anexo'
    accepts_nested_attributes_for :sip_anexo,  reject_if: :all_blank

  end
end


