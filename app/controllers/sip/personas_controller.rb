require 'sip/concerns/controllers/personas_controller'

module Sip
  class PersonasController < Sip::ModelosController

    load_and_authorize_resource class: Sip::Persona

    include Sip::Concerns::Controllers::PersonasController

   
    def atributos_show
      self.atributos_show_sip +
        [:anexos]
    end


    def lista_params
      lista_params_sip + [
        anexo_persona_attributes: [
          :id, 
          :persona_id,
          :_destroy,
          anexo_attributes: [
            :id, 
            :descripcion, 
            :adjunto, 
            :_destroy
          ]
        ]
      ]
    end

  end
end
